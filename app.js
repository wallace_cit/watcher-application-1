'use strict';

var express = require('express');
var expressValidator = require('express-validator');
var session = require('express-session');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require("./libs/logger");
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var morgan = require('morgan');

// default to a 'localhost' configuration:
var mongoDbUrl = 'localhost:27017/watcher';

// if OPENSHIFT env variables are present, use the available connection info:
if (process.env.OPENSHIFT_MONGODB_DB_PASSWORD) {
  mongoDbUrl = process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +
    process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
    process.env.OPENSHIFT_MONGODB_DB_HOST + ':' +
    process.env.OPENSHIFT_MONGODB_DB_PORT + '/' +
    process.env.OPENSHIFT_APP_NAME;
}

mongoose.connect(mongoDbUrl, { promiseLibrary: require('q') });
var db = mongoose.connection;
db.on('error', function(err) {
  logger.error(`Mongoose default connection error: ${err}`);
});
db.once('open', function() {
  logger.info(`Mongo connected correctly to server ${mongoDbUrl}`);
});

// express
var app = express();

// start listener
const server = app.listen(process.env.NODE_PORT || 3000, process.env.NODE_IP || 'localhost', function() {
  const host = server.address().address;
  const port = server.address().port;
  logger.info(`Application worker (${process.pid}) http://${host}:${port} started...`);
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
  resave: true, saveUninitialized: true,
  secret: 'uwotm8'
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(expressValidator());

app.use(morgan(' [ :logged-user - :date[iso] ]. Request [ :url ] ', { stream: logger.stream }));
morgan.token('logged-user', function getRemoteUserToken(req) {

  var loggedUser = req.session ? req.session.name : 'xpto';
  return loggedUser;

});

// routes
app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));
app.use('/images', require('./routes/images'));
app.use('/authentication', require('./routes/authentication'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// catch 422 and forward to error handler
app.use(function(err, req, res, next) {
  if (err.name && err.name === 'ValidationError') {
    var error = new Error('Unprocessable Entity');
    error.status = 422;
    error.details = err.errors;
    next(error);
  } else {
    next(err);
  }
});

// catch generic error handler
app.use(function(err, req, res, next) {

  if (res.headersSent) {
    return next(err);
  }

  var result = {
    message: err.message,
    status: err.status || 500,
    details: err.details
  };

  logger.error(req.originalUrl, `Status: ${result.status} Error: ${result.message}`);

  if (app.get('env') === 'development') {
    result.stack = err.stack;
  }

  res.status(result.status);

  var contype = req.headers['content-type'];
  if (!contype || contype.indexOf('application/json') !== 0) {
    res.render('error', result);
  } else {
    res.json(result);
  }

});

module.exports = app;
