'use strict';

const userService = require('../services/userService');

function isAuthenticated(req, res, next) {

  var username = "";

  if (req.session.user) {

    username = req.session.user.name;

    userService.findUserByUsername(username, function(err, dados) {
      if (err) return res.status(401).send({ message: 'Acesso negado' });
      req.session.user = dados;
      next();
    });
  } else {
    return res.status(401).send({
      message: 'Acesso negado'
    });
  }
}

function isAdminAuthenticated(req, res, next) {
  isAuthenticated(req, res, function() {
    if (req.session.user.admin) {
      next();
    } else {
      res.status(403).send({
        message: 'Acesso negado'
      });
    }
  });
}

module.exports = {
  isAuthenticated: isAuthenticated,
  isAdminAuthenticated: isAdminAuthenticated
};
