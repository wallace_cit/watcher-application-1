# Watcher Application

## Descrição

Projeto NodeJS Watcher Application

## Links

* [Slack](https://treinamento-nodejs.slack.com)
* [Bitbucket](http://dimas.cit:9001/bimodal/ICCD/new/master?file_name=README.md)
* [Shippable](https://app.shippable.com/subscriptions/56d862009d043da07b31e963/ci/dashboard)
* [Application](http://watcher-andreluiznsilva.rhcloud.com/) 
* [Monitor](http://watcher-andreluiznsilva.rhcloud.com/) 
* [MongoDB (Database: watcher User: admin Password: _-G1lh1Yrw36)](https://watcher-andreluiznsilva.rhcloud.com/rockmongo)

## Setup

Pra configurar o ambiente local:

1. Clone o repositório:
`git clone git@bitbucket.org:andreln/watcher.git`
    
2. Rodar o script de instalação do NodeJS e outras ferramentas [./scrips/nodejs-install.sh](./scripts/install.sh)

4. Dentro da pasta application e monitor, execute:
```
$ npm install
```
4. Dentro da pasta application e monitor, para iniciar a aplicação execute:
```
$ npm server
```
ou
```
$ node app
```
ou
```
$ nodemon app
```
5. Acessar [http://localhost:3000](http://localhost:3000)

## Ferramentas

As principais ferramentas utilizadas no desenvolvido e execução da aplicação.

* [Node 6.6](https://nodejs.org/en/)
* [NPM 3.5.2](https://www.npmjs.com/)
* [Gulp 3.9.1](http://gulpjs.com/)
* [Mocha 3.0.2](https://mochajs.org/)
* [Istanbul 1.1.1](https://www.npmjs.com/package/istanbul)

## IDEs

Não tem nenhum IDE definida como padrão para o projeto, pode usar a que achar melhor. Entre as opções temos:

* [Visual Studio Code](https://code.visualstudio.com/)
* [Brackets](http://brackets.io/)
* [Atom](https://atom.io/)
* [WebStorm](https://www.jetbrains.com/webstorm/)
* [IDEA-IntelliJ](https://www.jetbrains.com/idea/)

## Fluxo de trabalho

O fluxo de trabalho consiste no fluxo do Git Flow, onde os nomes das features são as histórias cadastradas no Jira.
Veja os seguintes tutorias para exemplos de como trabalhar com git flow:

* [Tutorial 1](https://fjorgemota.com/git-flow-uma-forma-legal-de-organizar-repositorios-git/)
* [Tutorial 2](https://www.atlassian.com/git/tutorials/comparing-workflows/)
* [Tutorial 3](http://jeffkreeftmeijer.com/2010/why-arent-you-using-git-flow/)
* [Tutorial 4](http://danielkummer.github.io/git-flow-cheatsheet/)
