'use strict';

const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const defs = require('../models/definitions');

const schema = new mongoose.Schema({

  name: defs.requiredString,
  content: defs.requiredString, // Base64
  datetime: defs.requiredDate
}, defs.options);

schema.plugin(mongoosePaginate);

module.exports = mongoose.model('Image', schema);
