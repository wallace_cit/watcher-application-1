'use strict';

module.exports = {

  requiredString: {
    type: String,
    required: true,
    minlength: 6,
    maxlength: 255
  },

  requiredPassword: {
    type: String,
    required: true,
    bcrypt: true
  },

  requiredBoolean: {
    type: Boolean,
    default: false
  },

  requiredNumber: {
    type: Number,
    required: true
  },

  requiredDate: {
    type: Date,
    required: true
  },

  options: {
    timestamps: true,
    versionKey: 'version',
    toJSON: {
      transform: function(doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
      }
    }
  }

};
