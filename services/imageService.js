'use strict';

const Image = require('../models/image');
const validator = require('validator');
const _ = require('lodash');
const logger = require('../libs/logger');

const encodedError = "The image is not Base64 encoded.";
const invalidBodyError = "Invalid body of image.";

function saveImage(image, callback) {

  if (_.isNull(image) || _.isEmpty(image)) {
    logger.error(`Error: [ ${invalidBodyError} ]`);
    return callback(new Error(invalidBodyError), null);
  }

  if (!validator.isBase64(image.content)) {
    logger.error(`Error: [ ${encodedError} ]`);
    return callback(new Error(encodedError), null);
  }

  Image.create(image, function(err, result) {
    if (err || !result) callback(err, null);
    logger.info(`Imagem ${image.toString()} criada com sucesso`);
    callback(null, result);
  });

}

function getImages(filters, callback) {

  var limit = filters.limit || 10;
  var offset = filters.offset || 0;

  var query = {};

  if (filters.limit && Number(filters.limit) && (filters.limit > 0)) {
    limit = filters.limit;
  } else {
    limit = 10;
  }

  if (filters.offset && Number(filters.offset) && (filters.offset >= 0)) {
    offset = filters.offset;
  } else {
    offset = 0;
  }

  if (filters.name) {
    query.name = filters.name;
  }

  if (filters.date) {
    query.datetime = new Date(filters.date);
  } else {

    if (filters.startDate === undefined && filters.endDate === undefined) {
      logger.error(`Error: [ Send both start and end date ]`);
      return callback(new Error('Send both start and end date'), null);
    }

    query.datetime = {
      $gte: filters.startDate,
      $lte: filters.endDate
    };
  }

  Image.paginate(query, { offset: offset, limit: limit }, function(err, result) {

    if (err) return callback(err, null);

    result.docs = result.docs.map(function(pImage) {
      pImage.contentUrl = '/image?name=' + pImage.name;
      pImage.content = undefined;
      return pImage;
    });

    logger.info(`Response da lista de imagems com sucesso`);
    return callback(null, result);

  });
}

function getImageById(id, callback) {

  if (!id.match(/^[0-9a-fA-F]{24}$/)) {
    logger.error(`Error: [ ${id} + ' it is not a valid id' ]`);
    return callback(new Error(id + ' it is not a valid id'), null);
  }

  Image.findById(id, function(err, result) {

    if (err) return callback(err, null);

    if (result && Object.keys(result) !== 0) {

      var contentUrl = '/' + result._id;
      var currentImage = _.merge(_.omit(result, 'name', 'content', 'datetime'), { contentUrl: contentUrl });

      logger.log('info', `Response de imagem com id ' + ${id} + ' e url ${currentImage.contentUrl}`);
      return callback(null, currentImage);

    }

    logger.log('error', `Nenhuma imagem com id ' + ${id} + ' foi encontrada`);
    return callback(new Error('Nenhuma imagem com id ' + id + ' foi encontrada'), null);

  });

}

function getImageContentById(params, callback) {

  var imageId = params.id;
  var imageParamType = params.format;

  if (_.isEmpty(imageId)) {
    logger.error('O id da imagem não foi enviado');
    return callback(new Error('O id da imagem não foi enviado'), null);
  }

  var isAValidFormat = (/(jpg|jpeg|png|bmp)$/i).test(imageParamType);

  if (!isAValidFormat) {
    logger.error('O formato da imagem não é válido. Os valores possíveis' +
                              ' são jpg, jpeg, png e bmp.');
    return callback(new Error('O formato da imagem não é válido. Os valores possíveis' +
      ' são jpg, jpeg, png e bmp.'), null);
  }

  Image.findById(imageId, function(err, result) {

    if (err) return callback(err, null);

    if (!_.isEmpty(result) && !_.isEmpty(result.content)) {

      var base64Image = result.content;

      // Remove the first part of the base64 string
      var reg = /^data:image\/png;base64,|^data:image\/jpeg;base64,|^data:image\/jpg;base64,|^data:image\/bmp;base64,/;

      var base64data = base64Image.replace(reg, '');

      var image = new Buffer(base64data, 'base64');

      logger.info(`Response de imagem com id ' + ${imageId} + ' e extensão ${imageParamType}`);
      return callback(null, { image: image, extension: imageParamType });

    }

    logger.error(`Nenhuma imagem com id ' + ${imageId} + ' foi encontrada`);
    return callback(new Error('Nenhuma imagem com id ' + imageId + ' foi encontrada'), null);
  });
}

module.exports = {
  saveImage: saveImage,
  getImages: getImages,
  getImageById: getImageById,
  getImageContentById: getImageContentById
};
