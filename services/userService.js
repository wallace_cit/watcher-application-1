'use strict';

const User = require('../models/user');
const logger = require('../libs/logger');

const saveUser = function(pUser, callback) {
  var user = {
    username: pUser.username,
    password: pUser.password,
    admin: pUser.admin
  };

  User.create(user, function(err, result) {
    if (err) return callback(err);
    if (!result) return callback(new Error('Ocorreu um erro ao adcionar usuario'));

    logger.info(`User ${result._id} | ${result.username} | ${user.admin} criado com sucesso`);

    callback(null, {
      id: result._id,
      username: result.username,
      admin: result.admin
    });
  });
};

const findUserByUsername = function(username, callback) {
  User.findOne({ username: username }, 'username admin', function(err, user) {
    if (err) return callback(err);
    if (!user) return callback(new Error('Usuario não encontrado'));

    logger.info(`User ${user._id} | ${user.username} | ${user.admin} encontrado com sucesso`);
    callback(null, {
      id: user._id,
      username: user.username,
      admin: user.admin
    });
  });
};

const login = function(username, password, callback) {
  User.findOne({ username: username }, function(err, user) {
    if (err) return callback(err);
    if (!user) return callback(new Error('Usuario não encontrado'));

    user.verifyPassword(password, function(perr, valid) {
      if (perr) return callback(perr);

      callback(null, {
        id: user._id,
        username: user.username,
        admin: user.admin,
        auth: valid
      });
    });
  });
};

module.exports = {
  saveUser: saveUser,
  findUserByUsername: findUserByUsername,
  login: login
};
