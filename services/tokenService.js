'use strict';

var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('../libs/config');
var User = require('../models/user.js');

function authenticate(username, password, callback) {
  // find the user
  User.findOne({
    username: username
  }, function(err, user) {

    if (err) return callback(err, null);

    if (!user) {
      return callback('Authentication failed. User not found.', null);
    } else if (user) {

      // check if password matches
      if (user.password === password) {
        // if user is found and password is right
        // create a token
        if (user._doc) {
          user = user._doc;
        }
        createToken(user, function(err, token) {
          // return the information including token as JSON
          callback(err, token);
        });
      } else {
        callback('Authentication failed. Wrong password.', null);
      }
    }
  });
}

function createToken(user, callback) {
  // valida campos do usuario necessarios para geracao de token
  var expirationTime = config.expirationTime;
  var superSecret = config.superSecret;
  if (!expirationTime) {
    return callback(new Error('Missing expiration time configuration.'), null);
  } else if (!superSecret) {
    return callback(new Error('Missing super secret configuration.'), null);
  }
  var username = "{\"username\":\"" + user.username + "\"}";
  var token = jwt.sign(JSON.parse(username), superSecret,
    { expiresIn: expirationTime }
  );
  callback(null, token);
}

module.exports = {
  authenticate: authenticate,
  createToken: createToken
};
