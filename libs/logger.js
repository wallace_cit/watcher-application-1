var winston = require('winston');
var path = require('path');
winston.emitErrs = true;

var logger = new winston.Logger({
  transports: [
    new winston.transports.File({
      level: 'info',
      filename: path.resolve(__dirname, 'info.log'),
      handleExceptions: true,
      json: true,
      maxsize: 5242880,
      maxFiles: 5,
      colorize: false
    }),
    new winston.transports.File({
      name: 'error-file',
      filename: path.resolve(__dirname, 'error.log'),
      level: 'error',
      maxsize: 5242880,
      maxFiles: 5,
      colorize: false,
      handleExceptions: true,
      json: true
    }),
    new winston.transports.Console({
      level: 'error',
      handleExceptions: true,
      json: false,
      colorize: true
    })
  ],
  exitOnError: false
});

module.exports = logger;
module.exports.stream = {
  write: function(message, encoding) {
    logger.info(message);
  }
};
