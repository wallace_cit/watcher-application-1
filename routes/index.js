'use strict';

const index = require('express')();
// const usuario = require('../middleware/userAuthentication');
const userService = require('../services/userService');

index.route('/')

.get(function(req, res) {
  res.redirect('index.html');
})

.post(function(req, res, next) {
  userService.login(req.body.username, req.body.password, function(err, result) {
    if (err) return next(err);
    res.send(result);
  });
});

index.route('/*.html')
  .get(function(req, res, next) {
    var view = req.url.substr(1, req.url.length - 6);
    res.render(view, {
      title: 'Watcher Application'
    }, function(err, html) {
      if (!html) return next();
      if (err) return next(err);
      res.send(html);
    });
  });

module.exports = index;
