const httpMocks = require('node-mocks-http');
const sinon = require('sinon');
const usuario = require('../../middleware/userAuthentication.js');
require('chai').should();
const userService = require('../../services/userService');

describe('Deve verificar a autenticação de', function() {

  describe('um usuário ', function() {

    var mock = {};

    beforeEach(function() {
      mock = sinon.mock(userService);
    });

    afterEach(function() {
      mock.restore();
    });

    it('que está autorizado', function(done) {
      var res = httpMocks.createResponse();
      var req = httpMocks.createRequest();

      var user = { name: 'Na', admin: true };
      var finalUser = { name: 'Na', admin: true };

      mock.expects('findUserByUsername').once().withArgs(user.name).yields(null, finalUser);

      req.session = { user: user };

      usuario.isAuthenticated(req, res, function() {
        mock.verify();
        res.statusCode.should.equal(200);
        done();

      });
    });

    it('que não está autorizado', function(done) {
      var res = httpMocks.createResponse();
      var req = httpMocks.createRequest();
      var spy = sinon.spy();

      var user = { name: 'Name Test', admin: true };

      mock.expects('findUserByUsername').withArgs("Name_test").yields(new Error('Error: Acesso negado'), null);

      req.session = user;

      sinon.assert.neverCalledWith(spy);
      usuario.isAuthenticated(req, res, spy);
      res.statusCode.should.equal(401);
      done();
    });

  });

  describe('um usuário admin', function() {

    var mock = {};

    beforeEach(function() {
      mock = sinon.mock(userService);
    });

    afterEach(function() {
      mock.restore();
    });

    it('que está autorizado e é administrador', function(done) {
      var res = httpMocks.createResponse();
      var req = httpMocks.createRequest();

      var user = { name: 'Na', admin: true };
      var finalUser = { name: 'Na', admin: true };

      mock.expects('findUserByUsername').once().withArgs(user.name).yields(null, finalUser);

      req.session = { user: user };

      usuario.isAdminAuthenticated(req, res, function() {
        mock.verify();
        res.statusCode.should.equal(200);
        done();
      });
    });

    it('que está autorizado e não é administrador', function(done) {
      var res = httpMocks.createResponse();
      var req = httpMocks.createRequest();
      var callback = sinon.spy();

      var user = { name: 'Na', admin: false };
      var finalUser = { name: 'Na', admin: false };

      mock.expects('findUserByUsername').once().withArgs(user.name).yields(null, finalUser);

      req.session = { user: user };

      usuario.isAdminAuthenticated(req, res, callback);

      mock.verify();
      sinon.assert.neverCalledWith(callback);
      res.statusCode.should.equal(403);
      done();
    });

    it('que não está autorizado', function(done) {
      var res = httpMocks.createResponse();
      var req = httpMocks.createRequest();
      var spy = sinon.spy();

      var user = { name: 'Name Test', admin: true };

      mock.expects('findUserByUsername').withArgs('Name_test').yields(new Error('Error: Acesso negado'), null);

      req.session = user;

      usuario.isAdminAuthenticated(req, res, spy);
      sinon.assert.neverCalledWith(spy);
      res.statusCode.should.equal(401);
      done();
    });

  });
});
