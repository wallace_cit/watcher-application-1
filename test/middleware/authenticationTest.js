const httpMocks = require('node-mocks-http');
const sinon = require('sinon');
const jwt = require('jsonwebtoken');
const tokenAuthentication = require('../../middleware/tokenAuthentication.js');
require('chai').should();

describe('Deve verificar ', function() {

  describe('um token de um usuário ', function() {

    it("quando for válido", function(done) {

      var res = httpMocks.createResponse();
      var req = httpMocks.createRequest();

      var user = { nome: "Name Test", admin: true };

      var token = jwt.sign(user, "super_secret", {
        expiresIn: 86400
      });

      req.body.token = token;

      tokenAuthentication.isAuthenticated(req, res, function() {
        res.statusCode.should.equal(200);
        done();

      });
    });

    it("quando não fornecer um token", function(done) {
      var res = httpMocks.createResponse();
      var req = httpMocks.createRequest();
      var spy = sinon.spy();
      req.body.token = "";

      // sinon.assert.called(spy);
      sinon.assert.neverCalledWith(spy);
      tokenAuthentication.isAuthenticated(req, res, spy);
      res.statusCode.should.equal(403);
      done();
    });

    it("quando token for inválido", function(done) {
      var res = httpMocks.createResponse();
      var req = httpMocks.createRequest();
      var spy = sinon.spy();

      var user = { nome: "Name Test", admin: true };

      var token = jwt.sign(user, "super_secret", {
        expiresIn: 86400
      });

      var mock = sinon.mock(jwt);
      mock.expects('verify').withArgs(token, 'super_secret').yields(new Error('Error Desconhecido'), null);

      req.body.token = token;

      tokenAuthentication.isAuthenticated(req, res, spy);

      sinon.assert.neverCalledWith(spy);
      res.statusCode.should.equal(401);
      mock.restore();
      done();
    });
  });
});
