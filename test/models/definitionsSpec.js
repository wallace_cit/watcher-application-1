'use strict';

const chai = require('chai');
const assert = chai.assert;

const defs = require('../../models/definitions');

describe('Model Definitions - ', function() {

  describe('verificando o atributo', function() {

    it('requiredString se eh valido', function() {
      assert.instanceOf(defs.requiredString.type, Object.prototype.constructor);
    });

    it('requiredBoolean se eh valido', function() {
      assert.instanceOf(defs.requiredBoolean.type, Object.prototype.constructor);
    });

    it('requiredNumber se eh valido', function() {
      assert.instanceOf(defs.requiredNumber.type, Object.prototype.constructor);
    });

    it('requiredDate se eh valido', function() {
      assert.instanceOf(defs.requiredDate.type, Object.prototype.constructor);
    });

    it('options se eh valido', function() {
      var ret = { _id: null };
      defs.options.toJSON.transform(null, ret, null);
      assert.isUndefined(ret._id);
    });

  });

});
