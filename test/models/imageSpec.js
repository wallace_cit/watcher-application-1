'use strict';

const chai = require('chai');
const assert = chai.assert;

const Image = require('../../models/image');
const moment = require('moment');

describe('Image model', function() {

  var image = {};

  describe('All fields empty check', function() {

    before(function() {
      image = new Image();
    });

    it('name is undefined', function() {
      assert.isUndefined(image.name);
    });

    it('content is undefined', function() {
      assert.isUndefined(image.content);
    });

    it('datetime is undefined', function() {
      assert.isUndefined(image.datetime);
    });

    it('is invalid', function() {

      var error = image.validateSync();
      assert.isNotNull(error);
      assert.equal(3, Object.keys(error.errors).length);

    });

  });

  describe('All fields filled check', function() {

    before(function() {
      image = new Image({
        name: 'Picture01',
        content: '123456',
        datetime: new Date(2016, 10, 30)
      });
    });

    it('name is Picture01', function() {
      assert.equal('Picture01', image.name);
    });

    it('type of name is string', function() {
      assert.typeOf(image.name, 'string');
    });

    it('content is 123456', function() {
      assert.equal('123456', image.content);
    });

    it('type of content is string', function() {
      assert.typeOf(image.content, 'string');
    });

    it('date is 2016-10-30', function() {
      var expectedDate = new Date(2016, 10, 30);
      assert.equal(moment(expectedDate).valueOf(), moment(image.datetime).valueOf());
    });

    it('type of datetime is date', function() {
      assert.typeOf(image.datetime, 'date');
    });

    it('is a valid object', function() {
      assert.isUndefined(image.validateSync());
    });

  });

});
