'use strict';

const chai = require('chai');
const assert = chai.assert;

const sinon = require('sinon');
const userService = require('../../services/userService');
const UserModel = require('../../models/user');

describe('User Service', function() {

  describe('Salvando usuario', function() {

    var mockUserModel;

    before(function() {
      mockUserModel = sinon.mock(UserModel);
    });

    after(function() {
      mockUserModel.restore();
    });

    it('Validando fluxo sem erro', function(done) {

      var user = {
        username: 'test',
        password: 'test123',
        admin: false
      };

      mockUserModel.expects('create').withArgs(user).yields(null, user);

      userService.saveUser(user, function(err, puser) {
        assert.isNull(err);
        assert.equal(puser.username, user.username);
        assert.isUndefined(puser.password);
        assert.equal(puser.admin, user.admin);
        done();
      });

    });

    it('Validando fluxo com erro', function(done) {

      var user = {
        username: 'test',
        password: 'test123',
        admin: false
      };

      mockUserModel.expects('create').withArgs(user).yields(new Error('Fluxo com error'), null);

      userService.saveUser(user, function(err, puser) {
        assert.isNotNull(err);
        assert.include(err.message, 'Fluxo com error');
        assert.isUndefined(puser);
        done();
      });

    });

    it('Validando usuario não retornado', function(done) {

      var user = {
        username: 'test',
        password: 'test123',
        admin: false
      };

      mockUserModel.expects('create').withArgs(user).yields(null, null);

      userService.saveUser(user, function(err, puser) {
        assert.isNotNull(err);
        assert.include(err.message, 'Ocorreu um erro ao adcionar usuario');
        assert.isUndefined(puser);
        done();
      });

    });

  });

  describe('Buscando usuario por username', function() {

    var mockUserModel;

    before(function() {
      mockUserModel = sinon.mock(UserModel);
    });

    after(function() {
      mockUserModel.restore();
    });

    it('Validando fluxo sem erro', function(done) {

      var user = {
        username: 'test',
        password: 'test123',
        admin: false
      };

      mockUserModel.expects('findOne').withArgs(
        { username: user.username },
        'username admin')
        .yields(null, user);

      userService.findUserByUsername('test', function(err, puser) {
        assert.isNull(err);
        assert.equal(user.username, puser.username);
        assert.isUndefined(puser.password);
        assert.equal(user.admin, puser.admin);
        done();
      });

    });

    it('Validando fluxo com erro', function(done) {

      var user = {
        username: 'test',
        password: 'test123',
        admin: false
      };

      mockUserModel.expects('findOne').withArgs(
        { username: user.username },
        'username admin')
        .yields(new Error('Ocorreu um erro'), null);

      userService.findUserByUsername('test', function(err, puser) {
        assert.isNotNull(err);
        assert.include(err.message, 'Ocorreu um erro');
        assert.isUndefined(puser);
        done();
      });

    });

    it('Validando usuario não encontrado', function(done) {

      var user = {
        username: 'test',
        password: 'test123',
        admin: false
      };

      mockUserModel.expects('findOne').withArgs(
        { username: user.username },
        'username admin')
        .yields(null, null);

      userService.findUserByUsername('test', function(err, puser) {
        assert.isNotNull(err);
        assert.include(err.message, 'Usuario não encontrado');
        assert.isUndefined(puser);
        done();
      });

    });

  });

  describe('Efetuando login', function() {

    var mockUserModel;

    before(function() {
      mockUserModel = sinon.mock(UserModel);
    });

    after(function() {
      mockUserModel.restore();
    });

    it('Validando fluxo sem erro com validação de senha ok', function(done) {

      var user = {
        username: 'test',
        password: 'test123',
        admin: false,
        verifyPassword: function(pass, callback) {
          callback(null, true);
        }
      };

      mockUserModel.expects('findOne').withArgs(
        { username: user.username })
        .yields(null, user);

      userService.login(user.username, user.password, function(err, puser) {
        assert.isNull(err);
        assert.equal(user.username, puser.username);
        assert.isUndefined(puser.password);
        assert.equal(user.admin, puser.admin);
        assert.isTrue(puser.auth);
        done();
      });

    });

    it('Validando fluxo sem erro com validação de senha not ok', function(done) {

      var user = {
        username: 'test',
        password: 'test123',
        admin: false,
        verifyPassword: function(pass, callback) {
          callback(null, false);
        }
      };

      mockUserModel.expects('findOne').withArgs(
        { username: user.username })
        .yields(null, user);

      userService.login(user.username, user.password, function(err, puser) {
        assert.isNull(err);
        assert.equal(user.username, puser.username);
        assert.isUndefined(puser.password);
        assert.equal(user.admin, puser.admin);
        assert.isFalse(puser.auth);
        done();
      });

    });

    it('Validando fluxo com erro na validação de senha', function(done) {

      var user = {
        username: 'test',
        password: 'test123',
        admin: false,
        verifyPassword: function(pass, callback) {
          callback(new Error('Ocorreu um erro'), null);
        }
      };

      mockUserModel.expects('findOne').withArgs(
        { username: user.username })
        .yields(null, user);

      userService.login(user.username, user.password, function(err, puser) {
        assert.isNotNull(err);
        assert.include(err.message, 'Ocorreu um erro');
        assert.isUndefined(puser);
        done();
      });

    });

    it('Validando fluxo com usuario não encontrado', function(done) {

      var user = {
        username: 'test',
        password: 'test123',
        admin: false
      };

      mockUserModel.expects('findOne').withArgs(
        { username: user.username })
        .yields(null, null);

      userService.login(user.username, user.password, function(err, puser) {
        assert.isNotNull(err);
        assert.include(err.message, 'Usuario não encontrado');
        assert.isUndefined(puser);
        done();
      });

    });

    it('Validando fluxo com erro', function(done) {

      var user = {
        username: 'test',
        password: 'test123',
        admin: false
      };

      mockUserModel.expects('findOne').withArgs(
        { username: user.username })
        .yields(new Error('Ocorreu um error'), null);

      userService.login(user.username, user.password, function(err, puser) {
        assert.isNotNull(err);
        assert.include(err.message, 'Ocorreu um error');
        assert.isUndefined(puser);
        done();
      });

    });

  });

});
