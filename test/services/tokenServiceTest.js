var chai = require('chai');
var assert = chai.assert;

const mongoose = require('mongoose');
const sinon = require('sinon');
const tokenService = require('../../services/tokenService');
const config = require('../../libs/config');

describe('Authenticate user', function() {

  var mock = {};

  describe('With valid user', function() {

    var validUser = {
      username: "admin",
      password: "admin",
      admin: "true"
    };

    before(function() {
      mock = sinon.mock(mongoose.model('User'));
      mock.expects('findOne').withArgs({
        username: validUser.username
      }).yields(null, validUser);
    });

    after(function() {
      mock.restore();
    });

    it('Return token', function() {

      tokenService.authenticate(validUser.username, validUser.password, function(err, result) {
        assert.isNull(err);
        assert(result);
        mock.verify();
      });
    });
  });

  describe('With invalid user', function() {

    var invalidUser = {
      username: "hacker",
      password: "admin",
      admin: "true"
    };

    before(function() {
      mock = sinon.mock(mongoose.model('User'));
      mock.expects('findOne').withArgs({
        username: invalidUser.username
      }).yields('Erro', null);
    });

    after(function() {
      mock.restore();
    });

    it('Return token', function() {

      tokenService.authenticate(invalidUser.username, invalidUser.password, function(err, result) {
        assert.isNull(result);
        assert(err);
        mock.verify();
      });
    });
  });

  describe('Configuration ', function() {

    var mock = {};

    var validUser = {
      username: "admin",
      password: "admin",
      admin: "true"
    };

    before(function() {
      mock = sinon.mock(mongoose.model('User'));
      mock.expects('findOne').withArgs({
        username: validUser.username
      }).yields(null, validUser);
    });

    after(function() {
      mock.restore();
    });

    it('Return error, if there is no superSecret configuration', function(done) {
      config.superSecret = null;
      tokenService.authenticate(validUser.username, validUser.password, function(err, result) {
        assert.isNull(result);
        assert(err);
        config.superSecret = 'super_secret';
        done();
      });
    });

    it('Return error, if there is no expiration time configuration', function(done) {
      config.expirationTime = null;
      mock.expects('findOne').withArgs({
        username: validUser.username
      }).yields(null, validUser);
      tokenService.authenticate(validUser.username, validUser.password, function(err, result) {
        assert.isNull(result);
        assert(err);
        config.expirationTime = '1d';
        done();
      });
    });
  });
});

