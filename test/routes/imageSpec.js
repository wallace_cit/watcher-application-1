'use strict';

const mongoose = require('mongoose');

const express = require('express');
const bodyParser = require('body-parser');
const request = require('supertest');
const chai = require('chai');
const assert = chai.assert;
const testProperties = require('../../testProperties');
const logger = require('../../libs/logger');
const fs = require('fs');
const path = require('path');

const sinon = require('sinon');
require('sinon-mongoose');

const imageService = require('../../services/imageService.js');

var app = express();
app.use(bodyParser.json());
app.set('view engine', 'hbs');
app.use('/', require('../../routes/images'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// catch 422 and forward to error handler
app.use(function(err, req, res, next) {
  if (err.name && err.name === 'ValidationError') {
    var error = new Error('Unprocessable Entity');
    error.status = 422;
    error.details = err.errors;
    next(error);
  } else {
    next(err);
  }
});

// catch generic error handler
app.use(function(err, req, res, next) {
  if (res.headersSent) {
    return next(err);
  }

  var result = {
    message: err.message,
    status: err.status || 500,
    details: err.details
  };

  logger.error(`Status: ${result.status} Error: ${result.message}`);

  if (app.get('env') === 'development') {
    result.stack = err.stack;
  }

  res.status(result.status);

  var contype = req.headers['content-type'];
  if (!contype || contype.indexOf('application/json') !== 0) {
    res.render('error', result);
  } else {
    res.json(result);
  }

});

describe('Integrated test', function() {

  var mock = null;

  var fileContent = new Buffer("Hello World").toString('base64');
  var bodyJson = {
    id: "41224d776a326fb40f000022",
    name: "tstImg",
    content: fileContent,
    type: "JPEG",
    datetime: "2015-01-01 00:00:00"
  };

  before(function() {
    mock = sinon.mock(mongoose.model('Image'));
    mock.expects('create').withArgs(bodyJson).yields(null, bodyJson);
    mock.expects('findById').withArgs(bodyJson.id).yields(null, bodyJson);
  });

  after(function() {
    mock.restore();
  });

  it('send a picture and search for it afterwards', function(done) {
    var imageId;

    request(app).post('/').send(bodyJson).end(function(err, res) {
      assert.isNull(err);
      assert.equal(200, res.status);
      imageId = res.body.id;

      request(app).get('/' + imageId).end(function(err, res) {
        assert.isNull(err);
        assert.equal(200, res.status);
        assert.equal(imageId, res.body.id);
        done();
      });
    });
  });

});

describe('Route image', function() {

  describe('/images', function() {

    var mock = {};

    before(function() {
      mock = sinon.mock(mongoose.model('Image'));
    });

    after(function() {
      mock.restore();
    });

    it('return {} if valid POST /', function(done) {

      var validImage = {
        name: "imageTest",
        content: "dGVzdGU=",
        type: "jpg",
        datetime: "2015-01-01 00:00:00"
      };

      mock.expects('create').withArgs(validImage).yields(null, validImage);

      request(app).post('/').send(validImage).end(function(err, res) {

        assert.isNull(err);
        assert.equal(JSON.stringify(validImage), res.text);
        assert.equal(200, res.status);

        mock.verify();
        done();

      });

    });

    it('return error if invalid Base 64 content in POST /', function(done) {

      var invalidContent = {
        name: "imageTest",
        content: "teste",
        type: "jpg",
        datetime: "2015-01-01 00:00:00"
      };

      request(app).post('/').send(invalidContent).end(function(err, res) {

        assert.isNull(err);
        assert.equal(500, res.status);
        assert.include(res.text, 'The image is not Base64 encoded');

        done();

      });

    });

    it('return error if invalid body in POST /', function(done) {

      var invalidContent = {};

      request(app).post('/').send(invalidContent).end(function(err, res) {

        assert.isNull(err);
        assert.equal(500, res.status);
        assert.include(res.text, 'Invalid body of image');

        mock.verify();
        done();

      });

    });

    it('should return array with image if valid GET', function(done) {

      const parameters = {
        name: "Image1",
        date: new Date(1475504920335)
      };

      const image = {
        name: "Image1",
        contentUrl: 'GET /image?name=Image1',
        datetime: new Date(1475504920335)
      };

      mock.expects('paginate').withArgs({
        name: parameters.name,
        datetime: parameters.date
      }, { offset: 0, limit: 10 }).yields(null,
      { docs: [image], total: 1, limit: 10, offset: 0 });

      request(app).get('/').query(parameters).end(function(err, res) {

        assert.isNull(err);
        assert.isNotNull(res.body[0]);
        assert.equal(image.name, res.body.docs[0].name);
        assert.equal(200, res.status);

        mock.verify();
        done();

      });

    });

    it('should return an error if request with empty parameters', function(done) {

      request(app).get('/').query({ name: 'Image1' }).end(function(err, res) {

        assert.isNull(err);
        assert.equal(500, res.status);
        assert.include(res.text, 'Send both start and end date');
        mock.verify();
        done();

      });

    });

    it('should return an error if the parameter is different from a hash code for the id', function(done) {
      var id = 1;
      request(app).get('/' + id).end(function(err, res) {
        assert.isNull(err);
        assert.equal(500, res.status);
        assert.include(res.text, id + ' it is not a valid id');
        mock.verify();
        done();

      });

    });

  });

  describe('/:id/:format', function(done) {

    var getImageContentById = sinon.spy(imageService, "getImageContentById");
    const contentBase64 = testProperties.base64Image;

    const imageId = '41224d776a326fb40f000001';
    const validImage = {
      _id: '41224d776a326fb40f000001',
      name: '41224d776a326fb40f000001',
      content: contentBase64,
      datetime: new Date()
    };

    var mock = {};

    before(function() {
      mock = sinon.mock(mongoose.model('Image'));
    });

    after(function() {
      mock.restore();
    });

    it('deve chamar o servico para recuperar um conteudo atraves do id passado', function(done) {

      mock.expects('findById').withArgs(imageId).yields(null, validImage);

      request(app).get('/' + imageId + '/jpg')
        .end(function(err, res) {

          assert.isNull(err);
          assert.notEqual(res.status, 404);
          sinon.assert.called(getImageContentById);
          getImageContentById.restore();
          done();

        });

    });

    it('deve retornar err se a imagem não contem o formato adequado', function(done) {

      mock.expects('findById').withArgs(imageId).yields(null, validImage);

      request(app).get('/' + imageId + '/piu')
        .end(function(err, res) {

          assert.isNull(err);
          assert.include(res.text, 'O formato da imagem não é válido');
          assert.notEqual(res.status, 404);
          done();

        });

    });

  });

  describe('/:id', function(done) {

    const imageId = '41224d776a326fb40f000001';
    const validImage = {
      _id: '41224d776a326fb40f000001',
      name: '41224d776a326fb40f000001',
      content: testProperties.base64Image,
      datetime: new Date()
    };

    var getImageById = {};
    var mockImage = {};

    before(function() {
      getImageById = sinon.spy(imageService, "getImageById");
      mockImage = sinon.mock(mongoose.model('Image'));
    });

    after(function() {
      getImageById.restore();
      mockImage.restore();
    });

    it('Validando uma requisição com sucesso', function(done) {

      mockImage.expects('findById').withArgs(imageId).yields(null, validImage);

      request(app).get('/' + validImage._id)
        .end(function(err, res) {
          assert.isNull(err);
          assert.equal(res.status, 200);
          assert.isObject(res.body);
          assert.deepEqual(res.body, { _id: validImage._id, contentUrl: '/' + validImage._id });
          sinon.assert.calledOnce(getImageById);
          done();
        });
    });

    it('Validando com um ID inválido', function(done) {

      request(app).get('/@@@@@@@@@')
        .end(function(err, res) {
          assert.isNull(err);
          assert.equal(res.status, 500);
          assert.include(res.text, 'is not a valid id');
          done();
        });
    });

    it('Validando com erro de banco de dados', function(done) {

      mockImage.expects('findById').withArgs(imageId).yields(new Error('key not found error'), null);

      request(app).get('/' + validImage._id)
        .end(function(err, res) {
          assert.isNull(err);
          assert.equal(res.status, 500);
          assert.include(res.text, 'key not found error');
          done();
        });
    });
  });

});

describe('Teste integrado validando logs', function() {

  var getImageById = {};
  var mockImage = {};

  before(function() {
    getImageById = sinon.spy(imageService, "getImageById");
    mockImage = sinon.mock(mongoose.model('Image'));
  });

  after(function() {
    getImageById.restore();
    mockImage.restore();
  });

  it('Deve inserir uma mensagem no erro de log', function(done) {

    const generatedId = Math.floor(Math.random() * 90000) + 10000;

    request(app).get('/' + generatedId).end(function(err, result) {

      if (!err) {

        var file = fs.createReadStream(path.resolve(__dirname, '../../libs/error.log'));
        file.on('data', function(data) {

          assert.include(data.toString('utf8'), generatedId + ' it is not a valid id');
          done();

        });

      }

    });

  });

});

